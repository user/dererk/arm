"""
Provides arm's version and release date.
"""

VERSION = '1.4.5.0'
LAST_MODIFIED = "April 28, 2012"

